# Created by mateu at 23.10.2021
Feature: Adding job openings
  # Adding new job openings to the marketplace - job openings

    Scenario: 2 - Adding new job opening
      Given the user visits '/marketplace/job-openings/list/?archived=false'
      And the user clicks on New job opening button
      When user fills all required fields
      And user clicks on 'Create Job Opening' button
      Then the list of job openings contains new job opening title