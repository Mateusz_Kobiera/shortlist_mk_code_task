from selenium import webdriver

from frontend.components.table import Table
from frontend.element_waiting import ElementWaiting
from frontend.elements.base_element import BaseElement
from frontend.elements.button import Button
from frontend.elements.checkbox import Checkbox
from frontend.elements.input import Input
from frontend.elements.switcher import Switcher


class BasePage(ElementWaiting):
    base_url = 'https://autotest.shortlist.co'
    login_url = base_url + '/api/users/login/n/d158411e86d749b682aed44d9871e715/'
    back_button = "//a[@class = 'nav-back']"

    def __init__(self, driver: webdriver):
        super().__init__(driver)
        self.driver = driver

    def get_element(self, locator: tuple) -> webdriver:
        """
        Choosing an element
        :param locator: xpath i type of element to get
        :return: element
        """
        self.logger.info(f'Trying to find element {locator}')
        if locator[1] == 'Button':
            element = Button(self.driver.find_element_by_xpath(locator[0]), locator[0])
        elif locator[1] == 'Input':
            element = Input(self.driver.find_element_by_xpath(locator[0]), locator[0])
        elif locator[1] == 'Checkbox':
            element = Checkbox(self.driver.find_element_by_xpath(locator[0]), locator[0])
        elif locator[1] == 'Switcher':
            element = Switcher(self.driver.find_element_by_xpath(locator[0]), locator[0])
        else:
            element = BaseElement(self.driver.find_element_by_xpath(locator[0]), locator[0])
        self.logger.info(f'Found element {locator}')
        return element

    def get_component(self, locator: tuple) -> webdriver:
        """
        Choosing a component
        :param locator: xpath i typ elementu do znalezienia
        :return: component
        """
        self.logger.info(f'Trying to find component {locator}')
        if locator[1] == 'Table':
            component = Table(self.driver.find_element_by_xpath(locator[0]), locator[0])
            self.logger.info(f'Found component {locator}')
        else:
            self.logger.error(f'Component {locator} not found')
            raise ValueError(f'There is no such a component as {locator[1]}')
        return component

    def go_back(self) -> None:
        """
        Go back from page - clicks on 'Back to...'
        """
        self.get_element((BasePage.back_button, "Button")).click()
