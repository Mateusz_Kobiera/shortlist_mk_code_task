from selenium import webdriver

from frontend.elements.base_element import BaseElement


class Button(BaseElement):
    """
    Button element, only to differ with BaseElement as text
    """
    def __init__(self, driver: webdriver, xpath: str):
        super().__init__(driver, xpath)

