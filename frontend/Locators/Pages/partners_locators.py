class PartnersLocators:
    add_partner_button = "//a[contains(@ng-click, 'addVendor()')]", "Button"
    vendor_items = "//vendor-item"
    partners_names = vendor_items + "//a[@class = 'vendor-item-name']", "Base"
