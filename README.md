# Shortlist code task by Mateusz Kobiera

# Installation and test run
1. Download Python3.9: https://www.python.org/downloads/release/python-397/
   Check option "Add to PATH" in installation
2. Download Chrome browser from https://www.google.com/intl/pl_pl/chrome/
3. Download chromedriver with the same version as browser: https://chromedriver.chromium.org/downloads
4. Put chromedriver.exe to specific directory e.g. C:\webdrivers
5. Add this directory to PATH
   Advance System settings - Environment Variables... - System variables - "Path" Edit...- New - Paste directory e.g."C:\webdrivers"
6. Clone shortlist_mk_code_task repository to some folder with Git Bash:
   git clone https://Mateusz_Kobiera@bitbucket.org/Mateusz_Kobiera/shortlist_mk_code_task.git
7. Set PYTHONPATH pointing to main directory of the repository:
   Advance System settings - Environment Variables - System variables - "PYTHONPATH" Edit... or New.. "PYTHONPATH" - Paste directory
8. Open cmd terminal and go to the repository main file and in terminal:
   1. Install requirements: pip install -r requirements.txt
   2. Create virtual environment:
      - "python -m venv test-env"
      - "test-env\Scripts\activate.bat"
   3. Run tests: pytest
9. Your report from tests will be available in Reports directory
