from copy import copy

from selenium import webdriver

from frontend.element_waiting import ElementWaiting


class Table(ElementWaiting):
    """
    Table component
    """
    head = "//div[@ng-class = 'column.styleClasses.headerCell']"
    row = "//div[@data-testid = 'sl-table-row']"

    def __init__(self, driver: webdriver, xpath: str):
        """
        :param driver:
        :param xpath:
        """
        super().__init__(driver)
        self.driver = driver
        self.xpath = xpath

    def get_head_elements(self) -> list:
        """
        Wybranie elementów nagłówka tabeli
        :return: lista elementów nagłówka
        """
        self.logger.info('Trying to find table head elements')
        return self.driver.find_elements_by_xpath(self.xpath + Table.head)

    def get_all_rows(self) -> list:
        """
        Get all rows elements
        :return: list of all rows elements
        """
        self.logger.info('Trying to find table rows elements')
        return self.driver.find_elements_by_xpath(self.xpath + Table.row)

    def get_row_elements(self, row_id: int) -> list:
        """
        Get all elements from a row
        :param row_id: row id (counted from 1)
        :return: List of elements in a row
        """
        self.logger.info(f'Trying to find elements in a table row with id {row_id}')
        return self.driver.find_elements_by_xpath(self.xpath + Table.row + f'[{row_id}]/div')

    def get_table(self, unique_column_name: str = None) -> dict:
        """
        Gets all table as dictionary with text values of elements in table
        :param unique_column_name: optional parameter, name of column for which we have unique values in rows
        (to set as rows keys in dict)
        :return: table as dictionary
        """
        table_heads = []
        for element in self.get_head_elements():
            table_heads.append(element.text)
        table_row = {}
        table = {}
        column_index = 0
        for row_id in range(0, len(self.get_all_rows())):
            for element in self.get_row_elements(row_id+1)[1:]:
                table_row[table_heads[column_index]] = element.text
                column_index += 1
            column_index = 0
            row_name = table_row[unique_column_name] if unique_column_name else row_id
            table[row_name] = copy(table_row)
        return table
