# Created by Mateusz at 23.10.2021
Feature: Adding partners
  # Adding partner to my Shortlist partners

  Scenario: 1 - Adding Partner
    Given the user clicks on Add Partner button
    When user fills all required fields (Name / Company, Email, check Short Onboarding checkbox)
    And mark 'Email partners and invite them to join your Shortlist account'
    And the user clicks on the 'Add to My Shortlist' button
    Then the list of partners contains new Partner data




