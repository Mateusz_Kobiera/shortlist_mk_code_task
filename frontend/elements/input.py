from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from frontend.elements.base_element import BaseElement


class Input(BaseElement):
    """
    Input element
    """
    def __init__(self, driver: webdriver, xpath: str):
        super().__init__(driver, xpath)

    def set_value(self, input_text: str, clear_input_before: bool = True) -> None:
        """
        Set value for inputs
        :param input_text: text to put into input
        :param clear_input_before: clears value before entering new one, True by default
        :return:
        """
        input_xpath = self.xpath
        input_element = self.get_base_element(input_xpath)
        if clear_input_before:
            input_element.driver.send_keys(Keys.CONTROL + "a")
            input_element.driver.send_keys(Keys.DELETE)
        input_element.driver.send_keys(input_text)
