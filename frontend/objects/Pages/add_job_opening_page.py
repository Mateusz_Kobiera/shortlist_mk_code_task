from selenium import webdriver

from frontend.Locators.Pages.add_condidates_locators import AddCandidatesLocators
from frontend.Locators.Pages.add_job_opening_locators import AddJobOpeningLocators
from frontend.objects.Pages.add_candidates_page import AddCandidatesPage
from frontend.objects.Pages.base_page import BasePage


class AddJobOpeningPage(BasePage):
    def __init__(self, driver: webdriver):
        super().__init__(driver)
        self.wait_for_element(AddJobOpeningLocators.job_title_input)

    def set_job_title(self, job_title: str) -> None:
        """
        Sets job title for new job opening
        :param job_title: job title for new job opening
        """
        self.get_element(AddJobOpeningLocators.job_title_input).set_value(job_title)

    def click_create_job_opening_button(self) -> AddCandidatesPage:
        """
        Clicks 'Create job opening' button and waits for redirection to Job openings page
        :returns: AddCandidatesPage driver
        """
        self.get_element(AddJobOpeningLocators.create_job_opening_button).click()
        return AddCandidatesPage(self.driver)
