from selenium import webdriver

from frontend.Locators.Pages.job_openings_locators import JobOpeningsLocators
from frontend.objects.Pages.add_job_opening_page import AddJobOpeningPage
from frontend.objects.Pages.base_page import BasePage


class JobOpeningsPage(BasePage):
    url = BasePage.base_url + '/marketplace/job-openings/list/?archived=false'

    def __init__(self, driver: webdriver):
        super().__init__(driver)
        self.wait_for_element(JobOpeningsLocators.job_openings_table)

    def click_new_job_opening_button(self) -> AddJobOpeningPage:
        """
        Clicks 'New job opening button'
        :return: AddJobOpeningPage object
        """
        self.get_element(JobOpeningsLocators.new_job_opening_button).click()
        return AddJobOpeningPage(self.driver)

    def get_job_openings_table(self) -> dict:
        """
        Gets job openings table as dict
        :return: dict
        """
        return self.get_component(JobOpeningsLocators.job_openings_table).get_table()

    def get_job_titles(self) -> list:
        """
        Gets job titles available in job openings table
        :return: list of job titles in job openings table
        """
        job_titles = []
        job_openings_table = self.get_job_openings_table()
        for job in job_openings_table:
            job_titles.append(job_openings_table[job]['Job title'])
        return job_titles
