from selenium import webdriver

from frontend.Locators.Pages.partners_locators import PartnersLocators
from frontend.objects.Pages.add_partners_page import AddPartnersPage
from frontend.objects.Pages.base_page import BasePage


class PartnersPage(BasePage):
    partners_url = BasePage.base_url + '/partners/shortlist/search/'

    def __init__(self, driver: webdriver):
        super().__init__(driver)
        self.wait_for_element(PartnersLocators.add_partner_button, timeout=10)

    def click_add_partner_button(self) -> AddPartnersPage:
        """
        Clicks add partner button
        :return: Add partners page
        """
        self.get_element(PartnersLocators.add_partner_button).click()
        return AddPartnersPage(self.driver)

    def _get_all_vendors(self) -> list:
        """
        Get all vendor items
        :return: list of all vendor items
        """
        return self.driver.find_elements_by_xpath(PartnersLocators.vendor_items)

    def get_all_vendor_names(self) -> list:
        all_vendor_items = self._get_all_vendors()
        all_vendor_names = []
        for _ in all_vendor_items:
            all_vendor_names.append(self.get_element(PartnersLocators.partners_names).get_text())
        return all_vendor_names

