from selenium import webdriver
from selenium.common.exceptions import StaleElementReferenceException

from frontend.element_waiting import ElementWaiting


class BaseElement(ElementWaiting):
    """
    Basic element
    """
    def __init__(self, driver: webdriver, xpath: str):
        """
        Basic element
        :param driver:
        :param xpath:
        """
        super().__init__(driver)
        self.driver = driver
        self.xpath = xpath

    def click(self) -> None:
        """
        Click on a element
        :return:
        """
        self.logger.info(f'Trying to click element with xpath {self.xpath}')
        try:
            self.driver.click()
        except StaleElementReferenceException:
            self.logger.warn('StaleElementReferenceException occurred. Trying to refresh driver.')
            self.driver.refresh()
            self.driver.click()
        self.logger.info(f'Element with xpath {self.xpath} was clicked')

    def get_text(self) -> str:
        """
        Get available text for an element
        :return:
        """
        self.logger.info(f'Trying to get text of element with xpath {self.xpath}')
        return self.driver.text

    def get_base_element(self, locator: str) -> webdriver:
        """
        Get base element, needed if we want get element which is ancestor/child of current element
        :param locator: xpath of element as a string
        :return: element driver
        """
        self.logger.info(f'Trying to get base element with xpath {self.xpath}')
        return BaseElement(self.driver.find_element_by_xpath(locator), locator)

