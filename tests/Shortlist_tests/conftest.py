import pytest

from frontend.objects.Pages.job_openings_page import JobOpeningsPage
from frontend.objects.Pages.partners_page import PartnersPage


@pytest.fixture
def partners_page(browser):
    return PartnersPage(browser)


@pytest.fixture
def job_openings_page(browser):
    browser.get(JobOpeningsPage.url)
    return JobOpeningsPage(browser)
