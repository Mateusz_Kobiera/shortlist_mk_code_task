import datetime
from pathlib import Path

import pytest
from selenium import webdriver

from frontend.objects.Pages.base_page import BasePage


@pytest.fixture
def browser() -> webdriver:
    """
    Open Chrome browser and log in to desired page
    """
    driver = webdriver.Chrome()
    page = BasePage(driver)
    driver.maximize_window()
    driver.delete_all_cookies()
    driver.get(page.login_url)
    yield driver
    driver.quit()


def pytest_html_report_title(report):
    """
    Name of html report file
    """
    report.title = "Shortlist tests"


def pytest_configure(config):
    """
    Configuration for report
    """
    main_dir: Path = Path(__file__).parent.parent
    reports_dir: Path = main_dir.joinpath('Reports')
    if not reports_dir.exists():
        reports_dir.mkdir()
    session_report_dir: Path = reports_dir.joinpath(f'{datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")}')
    session_report_dir.mkdir()
    config.option.htmlpath = session_report_dir.joinpath('Report.html')

