from selenium import webdriver

from frontend.elements.button import Button


class Switcher(Button):
    """
    Switcher (Toggle) element
    """
    def __init__(self, driver: webdriver, xpath: str):
        super().__init__(driver, xpath)
        self.is_active_path = self.xpath + '/div'

    def is_active(self) -> bool:
        """
        Checks if switcher is active
        :return: True if active
        """
        checkbox_class = self.get_base_element(self.is_active_path).driver.get_attribute('class')
        if 'active' in checkbox_class:
            return True
        else:
            return False
