from selenium import webdriver

from frontend.Locators.Pages.add_partners_locators import AddPartnersLocators
from frontend.Locators.Pages.partners_locators import PartnersLocators
from frontend.objects.Pages.base_page import BasePage


class AddPartnersPage(BasePage):
    def __init__(self, driver: webdriver):
        super().__init__(driver)
        self.wait_for_element(AddPartnersLocators.short_onboarding_checkbox)

    def select_short_onboarding(self) -> None:
        """
        Selects short onboarding checkbox if not selected before
        """
        short_onb_checkbox = self.get_element(AddPartnersLocators.short_onboarding_checkbox)
        if not short_onb_checkbox.is_checked():
            short_onb_checkbox.click()
    
    def set_name(self, name: str) -> None:
        """
        Sets name or company name for partner
        :param name: name/company of a partner
        """
        self.get_element(AddPartnersLocators.name_input).set_value(name)
        
    def set_email(self, email: str) -> None:
        """
        Sets email for partner
        :param email: email of a partner
        """
        self.get_element(AddPartnersLocators.email_input).set_value(email)

    def select_email_partners(self) -> None:
        """
        Selects email partners switcher if not selected before
        """
        email_partners_switcher = self.get_element(AddPartnersLocators.email_partners_switcher)
        if not email_partners_switcher.is_active():
            email_partners_switcher.click()

    def click_add_to_shortlist_button(self) -> None:
        """
        Clicks 'Add to My Shortlist' button and wait for redirection to All partners page
        """
        self.get_element(AddPartnersLocators.add_to_my_shortlist_button).click()
        self.wait_for_element(PartnersLocators.partners_names, timeout=10)
