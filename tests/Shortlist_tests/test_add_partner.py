from pytest_bdd import given, then, when, scenarios

scenarios('add_partners.feature')


@given("the user clicks on Add Partner button", target_fixture='add_partners_page')
def add_partners_page(partners_page):
    return partners_page.click_add_partner_button()


@when("user fills all required fields (Name / Company, Email, check Short Onboarding checkbox)")
def step_impl(add_partners_page):
    add_partners_page.set_name('Freddie Mercury')
    add_partners_page.set_email('freddie@abc.def')
    add_partners_page.select_short_onboarding()


@when("mark 'Email partners and invite them to join your Shortlist account'")
def step_impl(add_partners_page):
    add_partners_page.select_email_partners()


@when("the user clicks on the 'Add to My Shortlist' button")
def step_impl(add_partners_page):
    add_partners_page.click_add_to_shortlist_button()


@then("the list of partners contains new Partner data")
def step_impl(partners_page):
    assert 'Freddie Mercury' in partners_page.get_all_vendor_names()
