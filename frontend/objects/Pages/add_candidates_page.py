from selenium import webdriver

from frontend.Locators.Pages.add_condidates_locators import AddCandidatesLocators
from frontend.objects.Pages.base_page import BasePage


class AddCandidatesPage(BasePage):
    def __init__(self, driver: webdriver):
        super().__init__(driver)
        self.wait_for_element(AddCandidatesLocators.job_opening_title)

    def get_job_title(self) -> str:
        """
        Gets title of job opening in Add candidates page
        """
        return self.get_element(AddCandidatesLocators.job_opening_title).get_text()