class AddPartnersLocators:
    add_to_my_shortlist_button = "//span[text() = ' Add to My Shortlist ']/ancestor::a", "Button"
    name_input = "//input[@ng-model = 'newVendor.name']", "Input"
    email_input = "//input[@ng-model = 'newVendor.email']", "Input"
    select_workflows_input = "//input[@ng-model = '$ctrl.searchOnboardingTemplate']", "Input"
    short_onboarding_checkbox = \
        "//multistate-checkbox[@get-state= '$ctrl.getOnboardingTemplateState(onboardingTemplate)']", "Checkbox"
    email_partners_switcher = "//switcher[@ng-model = '$ctrl.isInvitation']", "Switcher"

