from selenium import webdriver

from frontend.elements.button import Button


class Checkbox(Button):
    """
    Checkbox element
    """
    def __init__(self, driver: webdriver, xpath: str):
        super().__init__(driver, xpath)
        self.is_checked_path = self.xpath + '//div'

    def is_checked(self) -> bool:
        """
        Checks if checkbox is checked (full)
        :return: True if checked
        """
        checkbox_class = self.get_base_element(self.is_checked_path).driver.get_attribute('class')
        if 'full' in checkbox_class:
            return True
        else:
            return False
