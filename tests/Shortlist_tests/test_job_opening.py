from pytest_bdd import given, then, when, scenarios

from frontend.Locators.Pages.job_openings_locators import JobOpeningsLocators

scenarios('add_job_openings.feature')


@given("the user visits '/marketplace/job-openings/list/?archived=false'")
def step_impl(job_openings_page):
    assert job_openings_page.url == job_openings_page.driver.current_url


@given("the user clicks on New job opening button", target_fixture='add_job_opening_page')
def add_job_opening_page(job_openings_page):
    return job_openings_page.click_new_job_opening_button()


@when("user fills all required fields")
def step_impl(add_job_opening_page):
    add_job_opening_page.set_job_title('QA')


@when("user clicks on 'Create Job Opening' button", target_fixture='add_candidates_page')
def add_candidates_page(add_job_opening_page):
    return add_job_opening_page.click_create_job_opening_button()


@then("the list of job openings contains new job opening title")
def step_impl(add_candidates_page, job_openings_page):
    assert add_candidates_page.get_job_title() == 'QA'
    add_candidates_page.go_back()
    job_openings_page.wait_for_element(JobOpeningsLocators.job_openings_table)
    assert 'QA' in job_openings_page.get_job_titles()
